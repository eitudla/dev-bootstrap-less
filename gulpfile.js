var gulp = require('gulp');
var less = require('gulp-less');
var path = require('path');

gulp.task('less', function () {
  gulp
    .src('./less/main.less') 
    .pipe(less({
      paths: ['./less']
    }))
    .pipe(gulp.dest('./css'));
});

gulp.task('default', function() {
  gulp.run('less');

  gulp.watch('./less/*.less', function(event) {
    gulp.run('less');
  });
});